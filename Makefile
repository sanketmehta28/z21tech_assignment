all: clean test build
clean:
	@echo "cleaning previous build"
 	$(shell find . -name *.pyc -delete)

test:
	@echo "testing the new build "

build:
	@echo "building new binary"

install:
	@echo "installing application"

uninstall:
	@echo "uninstalling application"

run: install
	@echo "running the application"
	@python count_school.py

git-hook:
	test -d .git/hooks || mkdir -p .git/hooks
	cp -f hooks/git-pre-commit.hook .git/hooks/pre-commit
	chmod a+x .git/hooks/pre-commit

.PHONY: all run uninstall clean
.DEFAULT_GOAL := all
