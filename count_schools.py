import csv

DATA_FILE = "school_data.csv"

class SchoolInfo:
    """Main class to store the data from csv"""
    def __init__(self):
        self.total_data = {}
        self.locale_info = {}
        self.city_with_most_school = None
        self.max_school_count = 0
        self.school_list = set()

    def parse_input(self, file_name):
        """
        Struct of total_data:  {'total': 10000,
                                <state_code>: {
                                    'total': 1000,
                                    <city_name>: {
                                        'total': 100,
                                        <school_name>: {...
                                            }
                                        }
                                    }
                                }
        """
        self.total_data.setdefault('total', 0)
        with open(file_name) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for record in csv_reader:

                # Add state information in total_data
                self.total_data.setdefault(record['LSTATE05'], {})
                self.total_data[record['LSTATE05']].setdefault('total', 0)

                # Add city info according to state
                self.total_data[record['LSTATE05']].setdefault(record['LCITY05'], {})
                self.total_data[record['LSTATE05']][record['LCITY05']].setdefault('total', 0)

                # Set max no of school and city which has it so searching time complexity is O(1)
                if self.total_data[record['LSTATE05']][record['LCITY05']]['total'] > self.max_school_count:
                    self.max_school_count = self.total_data[record['LSTATE05']][record['LCITY05']]['total']
                    self.city_with_most_school = record['LCITY05']

                # Add school info according to city and increment total count of school state and city vise
                if record['SCHNAM05'] not in self.total_data[record['LSTATE05']][record['LCITY05']]:
                    self.total_data[record['LSTATE05']][record['LCITY05']].setdefault(record['SCHNAM05'], set()).add(record['SCHNAM05'])
                    self.total_data['total'] += 1
                    self.total_data[record['LSTATE05']]['total'] += 1
                    self.total_data[record['LSTATE05']][record['LCITY05']]['total'] += 1

                # Add metro locale counts
                self.locale_info.setdefault(record['MLOCALE'], 0)
                self.locale_info[record['MLOCALE']] += 1

    def print_total_schools(self):
        print(f"Total schools: {self.total_data['total']}")

    def print_schools_per_state(self):
        print("Schools by state:")
        for state, state_info in sorted(self.total_data.items()):
            if state != 'total':
                print(f"{state}: {state_info['total']}")

    def print_school_per_metro_locale(self):
        print("Schools by Metro-centric locale:")
        for locale_code, total_schools in sorted(self.locale_info.items()):
            # Ignoring results where metro locale was not provided
            if locale_code != 'N':
                print(f"{locale_code}: {total_schools}")

    def print_city_with_most_school(self):
        print(f"City with most schools: {self.city_with_most_school} ({self.max_school_count} schools)")

    def print_unique_city(self):
        unique_cities = 0
        for state, state_info in self.total_data.items():
            if state != 'total':
                for city, city_info in state_info.items():
                    if city != 'total' and city_info['total'] != 0:
                        unique_cities += 1
        print(f"Unique cities with at least one school: {unique_cities}")
        print("FYI: same city name in diff states are considered different cities")

def print_schools(school_obj):
    try:
        school_obj.print_total_schools()
        print()
        school_obj.print_schools_per_state()
        print()
        school_obj.print_school_per_metro_locale()
        print()
        school_obj.print_city_with_most_school()
        print()
        school_obj.print_unique_city()
    except Exception as e:
        print(str(e))

def main():
    print("application is started")
    try:
        school_info = SchoolInfo()
        school_info.parse_input(DATA_FILE)
        print_schools(school_info)
    except Exception as e:
        print(str(e))
        exit(-1)

if __name__ == '__main__':
    main()