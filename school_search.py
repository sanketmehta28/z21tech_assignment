import time
from count_schools import DATA_FILE, SchoolInfo

# Global object used for storing data in memory
school_info_obj = None

def get_rank(search_str, school, city, state):
    """function to retrieve search rank based on relativity of search_str to school, city and state.
    It will return the avg rank"""
    school_rank = 0
    city_rank = 0
    state_rank = 0

    sub_str_list = search_str.split()
    for sub_str in sub_str_list:
        if sub_str in school:
            school_rank += 1
        if sub_str in city:
            city_rank += 1
        if sub_str in state:
            state_rank += 1
    return (school_rank + city_rank + state_rank)/3

def search_schools(search_str):
    """Main search function
    struct of result_set:
    {0: {(school1, city1, state1),(school2, city2, state2), ...},
     1: {(school1, city1, state1),(school2, city2, state2), ...},
     0.666: {(school1, city1, state1),(school2, city2, state2), ...},
     .
     .
     .
     }
    """
    result_set = {}

    for state, state_info in school_info_obj.total_data.items():
        state = state.lower()
        if state != 'total':
            for city, city_info in state_info.items():
                city = city.lower()

                # total is used to store the total no of schools in a state so ignoring it here
                if city != 'total':
                    for school, school_info in city_info.items():
                        school = school.lower()

                        # Total is used to store the total no of schools in a city so ignoring it here
                        if school != 'total':

                            # If we found the direct match, store it in result_set with 0 as a rank
                            if search_str == school or search_str == city or search_str == state:
                                result_set.setdefault(0, []).append((school, city, state))
                            else:
                                # Otherwise calculate the relative rank of the search_str and store in result_set
                                rank = get_rank(search_str, school, city, state)

                                # If no match then ignore the result
                                if not rank:
                                    continue
                                result_set.setdefault(rank, []).append((school, city, state))

    # Print the direct match first
    if 0 in result_set:
        for results in sorted(result_set):
            print(f"{results[0]}")
            print(f"{results[1]}, {results[2]}")

        # Remove direct match from result so that we can access all other results in next loop
        del result_set[0]

    # Printing all results except for one with lowest rank which can be ignored
    for rank in sorted(result_set, reverse=True)[:-1]:
            result = result_set[rank]
            for school, city, state in result:
                print(f"{school}")
                print(f"{city}, {state}")

if __name__ == '__main__':
    search_str = input("Provide the input to search: ")
    school_info_obj = SchoolInfo()
    school_info_obj.parse_input(DATA_FILE)
    start = time.time()
    search_schools(search_str.lower())
    print(f"total search time: {time.time() - start}s")